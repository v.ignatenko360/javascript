/*  
    свойства
    события
    методы
*/



//////////////////////////////////////////////////////////////////////////////////////////////////
/* 
    Переменная 
    Событие
*/
//////////////////////////////////////////////////////////////////////////////////////////////////

// var text = document.querySelector('#text'); //  text Переменная         //.querySelector СОБЫТИЕ Получение элемента но ID

// text.onclick = function () { /*     click – происходит, когда кликнули на элемент левой 
//                                     кнопкой мыши (на устройствах с сенсорными экранами
//                                     оно происходит при касании */
//     text.style.color = 'red';
//     text.innerHTML = "ура!";     // innerHTML СОБЫТИЕ Удаляет содиржимое блока и заполняет содержымым "" При += добавит содержымым ""
//     text.className = 'text';     // className СОБЫТИЕ При клике добавил класс "можно поменять"
//     text.style.fontSize = '30px';   //  СОБЫТИЕ
//     text.style.fontWeight = '400';     //  СОБЫТИЕ
// }
//////////////////////////////////////////////////////////////////////////////////////////////////
// // Все Функсии JS
// function print_object(obj) {
//     var res = '<ul>';

//     for(i in obj)
//         res += '<li><b>' + i + '</b>: ' +
//         obj[i] + '</li>';

//     res += '</ul>';
//     document.write(res);
// }

// var text = document.querySelector('#text'); //  text Переменная         //.querySelector Получение элемента но ID
// print_object(text);
//////////////////////////////////////////////////////////////////////////////////////////////////
// //  Все Стили
// function print_object(obj) {
//     var res = '<ul>';

//     for(i in obj)
//         res += '<li><b>' + i + '</b>: ' +
//         obj[i] + '</li>';

//     res += '</ul>';
//     document.write(res);
// }

// var text = document.querySelector('#text'); //  text Переменная         //.querySelector СОБЫТИЕ Получение элемента но ID
// print_object(text.style);
//////////////////////////////////////////////////////////////////////////////////////////////////
//  Переключение картинок через кнопки
// var btn_prev = document.querySelector('.gellery .buttons #prev');
// var btn_next = document.querySelector('.gellery .buttons #next');

// var images = document.querySelectorAll('.gellery .photos img');// All Все
// // console.log(images);
// var i = 0

// btn_prev.onclick = function() {
//     images[i].style.display = 'none';
//     // i = i - 1;
//     i--
//     if(i < 0) {     //  Если i меньше 0
//         i = images.length - 1;
//     }
//     images[i].style.display = 'block';
// }

// btn_next.onclick = function() {
//     images[i].style.display = 'none';
//     // i = i + 1;
//     i++
//     if(i >=images.length) {     //  Если i больше или ровно макс чеслу эле блока 
//         i = 0;
//     }
//     images[i].style.display = 'block';
// }
//////////////////////////////////////////////////////////////////////////////////////////////////
var btn_prev = document.querySelector('.gellery .buttons #prev');
var btn_next = document.querySelector('.gellery .buttons #next');

var images = document.querySelectorAll('.gellery .photos img');// All Все
// console.log(images);
var i = 0

btn_prev.onclick = function() {
    images[i].style.display = 'none';
    // i = i - 1;
    i--
    if(i < 0) {     //  Если i меньше 0
        i = images.length - 1;
    }
    images[i].style.display = 'block';
}

btn_next.onclick = function() {
    images[i].style.display = 'none';
    // i = i + 1;
    i++
    if(i >=images.length) {     //  Если i больше или ровно макс чеслу эле блока 
        i = 0;
    }
    images[i].style.display = 'block';
}
//////////////////////////////////////////////////////////////////////////////////////////////////
var prev_slider = document.querySelector('.gellery-slider .buttons-slider .prev');
var next_slider = document.querySelector('.gellery-slider .buttons-slider .next');

var images_slider = document.querySelectorAll('.gellery-slider .photos-slider img');// All Все
// console.log(images);
var i = 0

prev_slider.onclick = function() {
    images_slider[i].className = '';
    // i = i - 1;
    i--
    if(i < 0) {     //  Если i меньше 0
        i = images_slider.length - 1;
    }
    images_slider[i].className = 'showed';
}

next_slider.onclick = function() {
    images_slider[i].className = 'showed';
    // i = i + 1;
    i++
    if(i >=images_slider.length) {     //  Если i больше или ровно макс чеслу эле блока 
        i = 0;
    }
    images_slider[i].className = 'showed';
}
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
